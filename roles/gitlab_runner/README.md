Gitlab Runners
==============

Register a system as runner to Gitlab.

Distributions tested with Molecule:

 - CentOS 7/8
 - Debian 10 Buster
 - Ubuntu 20.04

Also tested on RHEL7/8. See below for limitations on CentOS/RHEL8.


Usage
-----

Used variables:

 - gitlab_runner_server: default https://gitlab.ethz.ch/
 - gitlab_runner_token: registration token taken from *Settings - CI/CD - Runners* in your group or project
 - gitlab_runner_locked: lock runner for current project, defaults to "false". Use hyphens, must be interpreted as string!
 - gitlab_runner_limit: maximum build trace size, default 0
 - gitlab_runner_concurrency: concurrent jobs, default 0
 - gitlab_runner_tags: comma separated list of tags, default ""
 - gitlab_runner_force: force registration even if runner is already registered
 - gitlab_runner_options: additional options, inserted as string to the registration command

Note that you can register a runner to multiple projects. `gitlab_runner_token` can be a simple string string:

```
gitlab_runner_token: ABCDEFG123456
```

but also a list of tokens

```
gitlab_runner_token: 
  - ABCDEFG123456
  - HIJKLMN345678
  - OPQRSTU678901
```


Limitations
-----------

 - CentOS/RHEL8: 
   - SELinux will be disabled during the installation. 
   - Docker is not well supported and may cause issues. If your pipeline jobs have to use
     containers, you should use podman - which may raise compatibility issues.