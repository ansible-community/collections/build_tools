#!/usr/bin/bash
gitlab-runner register -n --url={{ gitlab_runner_server }} --tag-list={{ gitlab_runner_tags|quote }} --executor=shell --registration-token={{ gitlab_runner_token|quote }} --locked={{ gitlab_runner_locked|quote }} --request-concurrency={{ gitlab_runner_concurrency|quote }} --limit={{ gitlab_runner_limit|quote }} {{ gitlab_runner_options }} && touch /etc/gitlab-runner/.configured
