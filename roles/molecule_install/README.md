Molecule Setup
==============

Install the molecule development framework for Ansible.

Purpose
-------

[Molecule](https://molecule.readthedocs.io/en/latest/) supports the
test driven development (TTD) of Ansible roles. You define
tests to ensure a role has performed its tasks as desired. Additionally, 
Molecule allows you to apply a role to different Linux distributions in parallel.


# Installation

## Supported Distributions

The role is tested on 

 - CentOS 8
 - Debian 10
 - Ubuntu 20.04

RHEL also works, but see the below how to add the Docker repository if using
RHEL 7.

## Note on CentOS 7

Molecule test has been suspended because of issues with Python 3.6
and encoding, see https://github.com/pypa/pip/issues/10219

This does not mean it does not work on CentOS 7. If you set 

```
LANG en_US.UTF-8
LC_ALL en_US.UTF-8
```
the issue is fixed. Given enough spare time, a updated test environment will be installed.

## Dependencies

Molecule uses Docker containers to spin up test clients. For RHEL7, there
are two ways to add a repository to provide the docker packages:

 1. You have a docker repo in Satellite. If you maintain the system
    via Satellite, this is the preferable option. Then you see updates listed in
    the Satellite web interface and can push install them. Add the
    repository to the host as usual: via activation key, subscription-manager
    or web interface.

 1. Otherwise set the Ansible variable `molecule_install_rhel_use_upstreamrepo`
    to `true` to use the same external repo as with CentOS 7.


## Optional Features

If a gitlab runner installation is detected because a user *gitlab-runner* exists, 
this user is added to the docker group. This allows the integration of Molecule
test in Gitlab CI/CD pipelines if the hosts uses the Docker daemon.
Disable this option with `molecule_install_detect_gitlabrunner` set to `false`.

# Molecule Drivers

Molecule uses drivers to provision test systems. This role installs the drivers
from PyPi:

 - `molecule-docker`: driver for the Docker daemon
 - `molecule-podman`: driver for podman
 - `molecule-vagrant`: driver for vagrant

See `roles/molecule_install/molecule` for examples for docker (with systemd enabled),
podman and vagrant.

Note that not always all drivers can handle all jobs. 

## molecule-docker
Default driver. Works with systemd controlled containers. For Ubuntu, Debian, EL7
and probably more.

## molecule-podman
For RHEL8 or Fedora, where Docker is not supported. There have been issues testing
EL7 in a podman environment, further research is necessary.

Avoid uppercase starting letters for instance names.

## molecule-vagrant
If a test needs a 'real' system environment, has to be rebooted during execution of
the role, if kernel tweaks have to be done, then
[molecule-vagrant](https://pypi.org/project/molecule-vagrant/) is an option. Most
easily deployed on your local hardware, because of problems stacking a VM in a VM
and containers in containers on top.

Configuration Example
---------------------

`molecule init role <rolename>` installs a default scenario configuration, which can be
edited and adapted (`<rolename>/molecule/default/molecule.yml`):

 - If there is a `create.yml`, remove it if you use one of the `molecule-*` drivers.

 - If you have to test roles which require access to systemd, other container images are required.
   Jeff Geerling maintains systemd enabled containers for most current (and not so current) OSes
   (see the examples mentioned above).

 - This Ansible module installs `yamllint` and `ansible-lint`, which can be added to the
   lint section. `ansible-lint` is highly recommended because it can detect ambigous constructs
   which can cause unexpected behavior.


An example for a configuration working well for this purpose is:

```
---
dependency:
  name: galaxy
driver:
  name: docker
lint: |
    set -e
    yamllint .
    ansible-lint -c molecule/default/ansible-lint.yml .
platforms:
  - name: el8
    image: geerlingguy/docker-centos8-ansible:latest
    command: ""
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
    privileged: true
    pre_build_image: True
  - name: debian10
    image: geerlingguy/docker-debian10-ansible:latest
    command: ""
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
    privileged: true
    pre_build_image: True
  - name: ubuntu2004
    image: geerlingguy/docker-ubuntu2004-ansible:latest
    command: ""
    volumes:
      - /sys/fs/cgroup:/sys/fs/cgroup:ro
    privileged: true
    pre_build_image: True
provisioner:
  name: ansible
verifier:
  name: ansible
```


# Testing
Since testing requires to build a stack of systems, with containers
in containers and other unusual setups, the most stable environment
is Vagrant. All tests for all OS, EL7/8, Debian and Ubuntu, pass the
tests without error.

In a gitlab-runner environment running on RHEL, only Debian and
Ubuntu test containers are able to be launched. And on Fedora 32
with podman, the EL7 container fails to start systemd services
reliably.

Sadly there is currently no gitlab-runner providing Vagrant/libvirt
service. The continuous testing for EL7/8 issues a
`molecule init testcase`, but launches no test client containers.
