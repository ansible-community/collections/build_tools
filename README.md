[![pipeline status](https://gitlab.ethz.ch/ansible-community/collections/build_tools/badges/master/pipeline.svg)](https://gitlab.ethz.ch/ansible-community/collections/build_tools/-/commits/master)
# Ansible Collection - ethz.build_tools

Roles for tools which support the implementation of build chains and pipelines
